package main;

import main.Card;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

    private int nextCardIndex;
    ArrayList<Card> deck = new ArrayList<Card>(52);

    public Deck(){

        int count = 0;
        for (int i = 1; i <= 13; i++) {
                deck.add(new Card('H',i));
            }
        for (int i = 1; i <= 13; i++) {
            deck.add(new Card('S',i));
            }
        for (int i = 1; i <= 13; i++) {
            deck.add(new Card('C',i));
            }
        for (int i = 1; i <= 13; i++) {
            deck.add(new Card('D',i));
            }

        Collections.shuffle(deck);

        nextCardIndex = 0;

    }

    public int getDeckSize()
    {
        return deck.size();
    }

    public Card nextCard() {

        return deck.get(nextCardIndex++);
    }

}
