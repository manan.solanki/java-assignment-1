package main;

public class Player {
    private String username;
    private int bankRoll ;
    private String email ;

    public Player() {
    }

    public Player(String username, int bankRoll, String email) {
        this.username = username;
        this.bankRoll = bankRoll;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getBankRoll() {
        return bankRoll;
    }

    public void setBankRoll(int bankRoll) {
        this.bankRoll = bankRoll;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void settle(int ammount){
        this.bankRoll+=ammount;
    }


}
