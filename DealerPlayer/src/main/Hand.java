package main;

import main.Card;

import java.util.Vector;

public class Hand {
    private Vector<Card> hand = new Vector<>();
    private int total = 0;
    private int aceInd = 0;

    public void addCard(Card card)
    {
        hand.add(card);
        total+=card.getValue();
        if(total>21)
        {
            for(int i = aceInd ; i < hand.size() && total>21 ; i++)
            {
                if(hand.get(i).getValue()==11)
                {
                    total-=10;
                    aceInd=i+1;
                }
            }
        }
    }

    public int getTotal()
    {
        return total;
    }

    public int getHandSize()
    {
        return hand.size();
    }
    public void printPlayerHand()
    {
        System.out.print("Player's hand : ");
        for(Card card : hand)
            System.out.print(card.printCard()+" -> ");
        System.out.println("Total :"+total);
    }

    public void printDealerHand()
    {
        System.out.print("Dealer's hand : ");
        for(Card card : hand)
            System.out.print(card.printCard()+" -> ");
        System.out.println("Total :"+total);
    }

    public void printDealerInitHand()
    {
        System.out.println("Dealer's hand : "+hand.get(0).printCard()+" -> ??????");
    }

    public void  clearHand()
    {
        hand.clear();
        total=0;
    }

}
