package main;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerList {

    private HashMap<String,Player> players = new HashMap<>();

    public PlayerList(){
        players.put("Iron man",new Player("Iron man",1500,"tony.stark@marvel.com"));
        players.put("Thor",new Player("Thor",10,""));
        players.put("Hulk",new Player("Hulk",500,"bruce.banner@marvel.com"));
        players.put("Black Widow",new Player("Black Widow",100,"natasha@marvel.com"));
        players.put("Captain America",new Player("Captain America",100,"steve.rogers@marvel.com"));
    }

    public Player findByUsername(String user)
    {
        return players.get(user);
    }


    public  boolean hasUser(String user)
    {

        if(players.get(user)!=null)
            return true;
        else
            return false;

    }

}
