package main.Test;

import main.Table;
import main.TableList;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TableListTest {

    @Test
    public void Test_hasTable()
    {
        TableList tableList = new TableList();
        String tname = "Earth";
        assertEquals(true,tableList.hasTable(tname));
        tname="Asgard";
        assertEquals(true,tableList.hasTable(tname));
        tname="Sapce";
        assertEquals(false,tableList.hasTable(tname));
    }

    @Test
    public void Test_findByTableName()
    {
        TableList tableList = new TableList();
        String tname = "Earth";
        Table table = tableList.findByTableName(tname);
        assertEquals(200,table.getBetSize());
        table = tableList.findByTableName("Titan");
        assertEquals(2000,table.getBetSize());
    }
}
