package main.Test;

import main.Player;
import main.PlayerList;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PlayerListTest {

    @Test
    public void Test_hasUser()
    {
        PlayerList playerList = new PlayerList();
        String uname="Captain America";
        assertEquals(true,playerList.hasUser(uname));
        uname="Thor";
        assertEquals(true,playerList.hasUser(uname));
        uname="Hawkye";
        assertEquals(false,playerList.hasUser(uname));
    }

    @Test
    public void Test_findByUsername()
    {
        PlayerList playerList = new PlayerList();
        String uname="Captain America";
        Player player=playerList.findByUsername(uname);
        assertEquals(100,player.getBankRoll());
        assertEquals("steve.rogers@marvel.com",player.getEmail());
    }


}
