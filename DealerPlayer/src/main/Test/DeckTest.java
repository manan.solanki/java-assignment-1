package main.Test;

import main.Card;
import main.Deck;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DeckTest {

    @Test
    public void Test_lengthOfDeck()
    {
        Deck deck = new Deck();
        assertEquals(52,deck.getDeckSize());
    }

    @Test
    public void Test_numberinSuit()
    {
        Deck deck = new Deck();
        int count = 0;
        for(int i=0;i<52;i++)
        {
            if(deck.nextCard().getSuit()=='D')
                count++;
        }
        assertEquals(13,count);//13 cards per suit
    }

    @Test
    public void Test_numberOfNumber()
    {
        Deck deck = new Deck();
        int count = 0;
        for(int i=0;i<52;i++)
        {
            if(deck.nextCard().getValue()==3)
                count++;
        }
        assertEquals(4,count);//4 card of one number
    }

    @Test
    public void Test_containsCard()
    {
        Deck deck = new Deck();
        int count = 0;
        for(int i=0;i<52;i++)
        {
            Card card= deck.nextCard();
            if(card.getSuitName()=="Hearts"&&card.getValueName()=="Ace")//Does this deck contain paricular card or not
                count++;
        }
        assertEquals(1,count);//Only one card in deck which is Ace of Hearts
    }

    @Test
    public void Test_uniqueCards()
    {
        Deck deck = new Deck();
        Card card1 = deck.nextCard();
        Card card2 = deck.nextCard();
        assertEquals(false,card1==card2);//Giving differnet card whenever call nextcard
    }
}
