package main.Test;

import main.Player;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void Test_createPlayer()
    {
        Player player = new Player("Leo",1000,"leo@xyz.com");
        assertEquals(1000,player.getBankRoll());
        assertEquals("leo@xyz.com",player.getEmail());
        player.settle(200);
        assertEquals(1200,player.getBankRoll());
    }
}
