package main.Test;

import main.Table;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TableTest {

    @Test
    public void Test_createTable()
    {
        Table table = new Table("Space",3000);
        assertEquals("Space",table.getName());
        assertEquals(3000,table.getBetSize());
    }
}
