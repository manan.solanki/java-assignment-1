package main.Test;

import main.Card;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CardTest {

    @Test
    public void Test_craeteCard()
    {
        Card card = new Card('S',11);
        assertEquals('S',card.getSuit());
        assertEquals(10,card.getValue());
        assertEquals("Spades",card.getSuitName());
        assertEquals("Jack",card.getValueName());
        Card card2 = new Card('C',1);//Ace has value 11
        assertEquals(11,card2.getValue());
    }

    @Test
    public void Test_printCard()
    {
        Card card = new Card('H',13);
        assertEquals("Hearts King",card.printCard());
    }


}
