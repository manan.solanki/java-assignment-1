package main.Test;

import main.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class GameTest {


    @Test
    public void Test_validateuser()
    {
        Game game = new Game();
        assertEquals("No user found!",game.validateUser("Thanos"));
        assertEquals("Hi Hulk, your bankroll is 500",game.validateUser("Hulk"));
    }

    @Test
    public void Test_validateTable()
    {
        Game game = new Game();
        Player player = new Player("Hulk",500,"dr.bruce@xyz.com");
        assertEquals("No table found!",game.validateTable("Space",player));
        assertEquals("Insufficient bank balance!",game.validateTable("Vormir",player));//bankroll = 500 < betsize  = 1000
        assertEquals("You have selected Earth with bet size 200",game.validateTable("Earth",player));
    }

    @Test
    public void Test_initializegame()
    {
        Game game = new Game();
        assertEquals("2 2",game.initializeGame());//both have 2 2 cards in hand
    }

    @Test
    public void Test_Hit()
    {
        Game game = new Game();
        Hand hand = new Hand();
        assertEquals(0,hand.getHandSize());
        game.Hit(hand);
        assertEquals(1,hand.getHandSize());//Add one card in hand
    }

    @Test
    public void Test_DealerTurn()
    {
        Game game = new Game();
        Hand hand = new Hand();
        assertEquals(0,hand.getTotal());
        game.DealerTurn(hand);
        assertEquals(true,hand.getTotal()>17);//Draw card untill total > 17
    }


    //Some scenarios of the game
    @Test
    public void Test_Scenario1()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',12));
        playerhand.addCard(new Card('H',9));
        playerhand.addCard(new Card('H',2));
        assertEquals(21,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',1));
        assertEquals(21,dealerhand.getTotal());
        //both wooed
        assertEquals("Game draw!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(500,player.getBankRoll());//winning 0

    }

    @Test
    public void Test_Scenario2()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',12));
        playerhand.addCard(new Card('H',9));
        playerhand.addCard(new Card('H',2));
        assertEquals(21,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',5));
        dealerhand.addCard(new Card('S',4));
        assertEquals(19,dealerhand.getTotal());
        //only player wooed
        assertEquals("Player won!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(700,player.getBankRoll());//Winning  = bet size

    }

    @Test
    public void Test_Scenario3()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',12));
        playerhand.addCard(new Card('H',9));
        assertEquals(19,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',5));
        dealerhand.addCard(new Card('S',8));
        assertEquals(23,dealerhand.getTotal());
        //Dealer busted
        assertEquals("Player won!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(700,player.getBankRoll());//Winning  = bet size

    }

    @Test
    public void Test_Scenario4()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',12));
        playerhand.addCard(new Card('H',9));
        assertEquals(19,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',4));
        dealerhand.addCard(new Card('S',6));
        assertEquals(20,dealerhand.getTotal());
        //dealer has higher total
        assertEquals("Dealer won!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(300,player.getBankRoll());//loss = bet size

    }

    @Test
    public void Test_Scenario5()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',1));
        playerhand.addCard(new Card('H',9));
        assertEquals(20,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',4));
        dealerhand.addCard(new Card('S',6));
        assertEquals(20,dealerhand.getTotal());
        //same total
        assertEquals("Game draw!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(500,player.getBankRoll());//No win/loss

    }

    @Test
    public void Test_Scenario6()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',10));
        playerhand.addCard(new Card('H',9));
        playerhand.addCard(new Card('H',8));
        assertEquals(27,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',4));
        dealerhand.addCard(new Card('S',6));
        assertEquals(20,dealerhand.getTotal());
        //Player busted
        assertEquals("Dealer won!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(300,player.getBankRoll());//loss = bet size

    }

    @Test
    public void Test_Scenario7()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',10));
        playerhand.addCard(new Card('H',9));
        playerhand.addCard(new Card('H',5));
        assertEquals(24,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',4));
        dealerhand.addCard(new Card('S',8));
        assertEquals(22,dealerhand.getTotal());
        //both busted
        assertEquals("Game draw!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(500,player.getBankRoll());// no win/loss

    }

    @Test
    public void Test_Scenario8()
    {
        Game game = new Game();
        Hand playerhand = new Hand();
        Hand dealerhand = new Hand();
        Player player = new Player("Hulk",500,"bruce.banner@marvel.com");
        Table table = new Table("Earth",200);
        game.validateTable("Earth",player);

        playerhand.addCard(new Card('H',12));
        playerhand.addCard(new Card('H',9));
        assertEquals(19,playerhand.getTotal());

        dealerhand.addCard(new Card('S',13));
        dealerhand.addCard(new Card('S',8));
        assertEquals(18,dealerhand.getTotal());
        //player has highe value
        assertEquals("Player won!",game.decideWinner(playerhand,dealerhand,player,table));
        assertEquals(700,player.getBankRoll());//Winning  = bet size

    }

}
