package main.Test;

import main.Card;
import main.Hand;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HandTest {

    @Test
    public void Test_createHand()
    {
        Hand hand = new Hand();
        assertEquals(0,hand.getTotal());
        assertEquals(0,hand.getHandSize());
    }

    @Test
    public void Test_addCard()
    {
        Hand hand = new Hand();
        hand.addCard(new Card('H',12));
        assertEquals(10,hand.getTotal());//Value 10 not 12
        assertEquals(1,hand.getHandSize());//One card in hand
        hand.addCard(new Card('H',9));
        hand.addCard(new Card('S',1));//Ace's value 1
        assertEquals(20,hand.getTotal());
        assertEquals(3,hand.getHandSize());
    }

    @Test
    public void Test_checkAceValue()
    {
        Hand hand = new Hand();
        hand.addCard(new Card('H',12));
        hand.addCard(new Card('H',1));//Total 21 , Ace value 11
        assertEquals(21,hand.getTotal());
        hand.addCard(new Card('S',1));//Total 12 , Both ace values 1
        assertEquals(12,hand.getTotal());
    }
    @Test
    public void Test_clearHand()
    {
        Hand hand = new Hand();
        hand.addCard(new Card('H',12));
        hand.addCard(new Card('H',10));
        assertEquals(2,hand.getHandSize());
        hand.clearHand();
        assertEquals(0,hand.getHandSize());//Remove all cards from hand,total zero
        assertEquals(0,hand.getTotal());
    }
}
