package main;

import java.util.Scanner;

public class Game {

    private Deck deck = new Deck();
    private PlayerList playerList = new PlayerList();
    private TableList tableList = new TableList();
    private Scanner sc = new Scanner(System.in);
    private Hand playerHand = new Hand();
    private Hand dealerHand = new Hand();
    private Player player;
    private Table table;

    //Some arguments and return type are set for testing purpose

    public String validateUser(String user)
    {
        if(playerList.hasUser(user))
        {
            player = playerList.findByUsername(user);
            return "Hi "+player.getUsername()+", your bankroll is "+player.getBankRoll();
        }
        else
            return "No user found!";
    }

    public String validateTable(String name,Player player)
    {
        if(tableList.hasTable(name))
        {
            table = tableList.findByTableName(name);
            if(table.getBetSize()>player.getBankRoll())
                return "Insufficient bank balance!";
            else
            {
                player.settle(-1*table.getBetSize());
                return  "You have selected " + table.getName() + " with bet size " + table.getBetSize();
            }

        }
        else
            return "No table found!";
    }

    public String initializeGame()//Draw two cards to both player and dealer
    {
        playerHand.addCard(deck.nextCard());
        playerHand.addCard(deck.nextCard());
        dealerHand.addCard(deck.nextCard());
        dealerHand.addCard(deck.nextCard());
        return playerHand.getHandSize()+" "+ dealerHand.getHandSize();
    }


    public String decideWinner(Hand playerHand,Hand dealerHand,Player player,Table table)
    {
        if(playerHand.getTotal()==21)
        {
            if(dealerHand.getTotal()==21)
            {
                player.settle(table.getBetSize());
                return "Game draw!";
            }
            else
            {
                player.settle(2*(table.getBetSize()));
                return "Player won!";
            }
        }
        else if(playerHand.getTotal()<21)
        {
            if(dealerHand.getTotal()>21)
            {
                player.settle(2*(table.getBetSize()));
                return "Player won!";
            }
            else if(dealerHand.getTotal()>playerHand.getTotal())
            {
                return "Dealer won!";
            }
            else if(playerHand.getTotal()>dealerHand.getTotal())
            {

                player.settle(2*(table.getBetSize()));
                return "Player won!";
            }
            else {

                player.settle(table.getBetSize());
                return "Game draw!";
            }
        }
        else
        {
            if(dealerHand.getTotal()>21)
            {
                player.settle(table.getBetSize());
                return "Game draw!";
            }
            else
            {
                return "Dealer won!";
            }
        }
    }


    public void Hit(Hand hand)
    {
        Card newCard = deck.nextCard();
        hand.addCard(newCard);
    }

    public void DealerTurn(Hand dealerHand)//Dealer's turn when player woo,busted or stand up
    {
        while(dealerHand.getTotal()<17)
        {
            Hit(dealerHand);
        }
    }

    public void clearHands()
    {
        playerHand.clearHand();
        dealerHand.clearHand();
    }

}
