package main;

public class Table {
    private String name;
    private  int betSize;

    public Table(String name, int betSize) {
        this.name = name;
        this.betSize = betSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBetSize() {
        return betSize;
    }

    public void setBetSize(int betSize) {
        this.betSize = betSize;
    }
}
