package main;

import java.util.ArrayList;
import java.util.HashMap;

public class TableList {
    HashMap<String,Table> tables = new HashMap<>();

    public TableList()
    {
        tables.put("Ego",new Table("Ego",100));
        tables.put("Earth",new Table("Earth",200));
        tables.put("Asgard",new Table("Asgard",500));
        tables.put("Vormir",new Table("Vormir",1000));
        tables.put("Titan",new Table("Titan",2000));
    }

    public Table findByTableName(String name)
    {
        return tables.get(name);
    }

    public  boolean hasTable(String name)
    {

        if(tables.get(name)!=null)
            return true;
        else
            return false;
    }

}
