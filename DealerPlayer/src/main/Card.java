package main;

public class Card {
    private char suit;
    private int value;

    public Card() {
    }

    public Card(char suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuitName() {

        String suit;
        if (this.suit == 'H') {
            suit = "Hearts";
        }
        else if (this.suit == 'S') {
            suit = "Spades";
        }
        else if (this.suit == 'C') {
            suit = "Clubs";
        }
        else
            suit = "Diamonds";
        return suit;
    }

    public String getValueName(){
        String name = "";
        if (this.value == 1) {
            name = "Ace";
        }
        else if (this.value == 2) {
            name = "Two";
        }
        else if (this.value == 3) {
            name = "Three";
        }
        else if (this.value == 4) {
            name = "Four";
        }
        else if (this.value == 5) {
            name = "Five";
        }
        else if (this.value == 6) {
            name = "Six";
        }
        else if (this.value == 7) {
            name = "Seven";
        }
        else if (this.value == 8) {
            name = "Eight";
        }
        else if (this.value == 9) {
            name = "Nine";
        }
        else if (this.value == 10) {
            name = "Ten";
        }
        else if (this.value == 11) {
            name = "Jack";
        }
        else if (this.value == 12) {
            name = "Queen";
        }
        else if (this.value == 13) {
            name = "King";
        }
        return name;
    }

    public char getSuit() {
        return suit;
    }

    public void setSuit(char suit) {
        this.suit = suit;
    }

    public int getValue() {
        if(value>=10)
            return 10;
        else if(value==1)
            return 11;
        else
            return value;

    }

    public void setValue(int value) {
        this.value = value;
    }

    public String printCard(){
        return this.getSuitName()+" "+this.getValueName();
    }

}
